import fetch from "node-fetch";

export async function request(url: string, body: any, headers = {}, method: string = "POST"): Promise<any> {
  const response = await fetch(url, {
    method,
    body: JSON.stringify(body),
    headers: {
      "content-type": "application/json",
      ...headers,
    },
  });

  const data = await response.json();

  if (response.status !== 200) {
    throw new Error(
      `Response status is ${response.status}, response data is ${JSON.stringify(data)}`,
    );
  }

  expect(response.status).toEqual(200);

  return data;
}
