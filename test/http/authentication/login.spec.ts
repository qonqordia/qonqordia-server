import { RegisterRequest } from "../../../src/presentation/http/authentication/register/register.request";
import { request } from "../../functions";

describe("register", () => {
  let promise;

  const email = "3f5e2e62-dd24-4f6a-8947-347b5c02f45a@a98090d7-f01f-44e6-b10c-0715b3347d97";

  beforeEach(async () => {
    promise = register({
      email,
    });
  });

  it("should return the newly created authenticator", async () => {
    const result = await promise;

    expect(result).toEqual(expect.objectContaining({
      id: expect.any(String),
      email,
    }))
  });

  async function register(body: RegisterRequest): Promise<any> {
    return request(
      "http://127.0.0.1:3000/register",
      {
        email: body.email,
      },
      {},
      "POST"
    );
  }
});
