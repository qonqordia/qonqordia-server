import { env } from './../src/infrastructure/env';
import { NestExpressApplication } from "@nestjs/platform-express";
import { startApplication } from '../src/app';

let app: NestExpressApplication;

export async function setup(): Promise<void> {
  app = await startApplication();
  
  app.listen(env().server.port);
}

export async function teardown(): Promise<void> {
  await app.close();
}
