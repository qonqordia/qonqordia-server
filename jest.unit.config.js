const config = require("./jest.config");

module.exports = {
  ...config,
  testRegex: "(src/app.module.spec.ts$)|(src/domain/.*.spec.ts$)",
};
