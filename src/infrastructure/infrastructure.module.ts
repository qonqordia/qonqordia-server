import { AuthenticatorRepositoryProvider } from './database/authenticator/authenticator-repository.provider';
import { Module } from "@nestjs/common";
import { ColorRepositoryProvider } from './database/color/color-repository.provider';

const providerList = [AuthenticatorRepositoryProvider, ColorRepositoryProvider];

@Module({
  exports: providerList,
  providers: providerList,
})
export class InfrastructureModule {}