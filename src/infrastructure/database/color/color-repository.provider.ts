import { InMemoryColorRepository } from './in-memory/in-memory-color.repository';
import { COLOR_REPOSITORY } from './../../../common/constants';
import { FactoryProvider } from "@nestjs/common";
import { ColorRepository } from '../../../domain/color/gateways/color.repository';

export const ColorRepositoryProvider: FactoryProvider<ColorRepository> = {
  provide: COLOR_REPOSITORY,
  useFactory: () =>
    new InMemoryColorRepository(),
};
