import { constVoid } from 'fp-ts/function';
import { option, taskEither } from "fp-ts";
import { Option } from "fp-ts/Option";
import { TaskEither } from "fp-ts/TaskEither";
import { Color } from "../../../../domain/color/entities/color";
import {
  ColorFilters,
  ColorRepository,
} from "../../../../domain/color/gateways/color.repository";

export class InMemoryColorRepository implements ColorRepository {
  constructor(private readonly colors: Color[] = []) {}

  public create(color: Color): TaskEither<never, void> {
    this.colors.push(color);

    return taskEither.of(constVoid());
  }

  public update(updatedColor: Color): TaskEither<never, void> {
    const foundColorIndex = this.colors.findIndex((color) => color.id === updatedColor.id);

    if (foundColorIndex > -1) {
      this.colors[foundColorIndex] = updatedColor;
    }
    
    return taskEither.of(constVoid());
  }

  public findOneByFilter(
    filters: ColorFilters
  ): TaskEither<never, Option<Color>> {
    return taskEither.of(
      option.fromNullable(
        this.colors.find((color) =>
          Object.keys(filters).reduce((acc, key) => {
            if (acc === true && filters[key] === color[key]) {
              return true;
            }
            return false;
          }, true)
        )
      )
    );
  }
}
