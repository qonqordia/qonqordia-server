import { NotFoundError } from "../../../../common/errors/not-found.error";
import { Authenticator } from "../../../../domain/authentication/entities/authenticator";
import { constVoid, pipe } from "fp-ts/function";
import { TaskEither } from "fp-ts/TaskEither";
import { option, taskEither } from "fp-ts";
import { InternalError } from "../../../../common/errors/internal.error";
import {
  AuthenticatorFilters,
  AuthenticatorRepository,
} from "../../../../domain/authentication/gateways/authenticator.repository";
import { DuplicateError } from '../../../../common/errors/duplicate.error';

export class InMemoryAuthenticatorRepository
  implements AuthenticatorRepository
{
  public authenticatorList: Authenticator[];

  constructor(authenticatorList: Authenticator[] = []) {
    this.authenticatorList = authenticatorList;
  }

  public create(properties: Authenticator): TaskEither<InternalError | DuplicateError, void> {
    return pipe(
      this.authenticatorList.find((authenticator) => authenticator.email === properties.email),
      option.fromNullable,
      option.fold(
        () => pipe(
          this.authenticatorList.push(properties),
          () => taskEither.of(constVoid()),
        ),
        (authenticator) => pipe(
          taskEither.left(new DuplicateError("Authenticator", { email: authenticator.email })),
        ),
      ),
    );
  }

  public getOneByFilter(
    filter: AuthenticatorFilters
  ): TaskEither<InternalError | NotFoundError, Authenticator> {
    return pipe(
      this.authenticatorList.find(
        (authenticator) => authenticator.email === filter.email
      ),
      option.fromNullable,
      option.fold(
        () => taskEither.left(new NotFoundError("Authenticator", filter)),
        (authenticator) => taskEither.right(authenticator)
      )
    );
  }
}
