import { AUTHENTICATOR_REPOSITORY } from './../../../common/constants';
import { AuthenticatorRepository } from '../../../domain/authentication/gateways/authenticator.repository';
import { FactoryProvider } from "@nestjs/common";
import { InMemoryAuthenticatorRepository } from './in-memory/in-memory-authenticator.repository';

export const AuthenticatorRepositoryProvider: FactoryProvider<AuthenticatorRepository> = {
  provide: AUTHENTICATOR_REPOSITORY,
  useFactory: () =>
    new InMemoryAuthenticatorRepository(),
};
