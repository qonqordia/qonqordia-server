export class NotFoundError extends Error {
  public static readonly CODE = "NOT_FOUND";
 
  public originalMessage: string;

  constructor(entity: string, criteria: Object) {
    super(NotFoundError.CODE);
    this.originalMessage = `Cannot find entity ${entity} with criteria ${JSON.stringify(criteria)}`;
  }
}
