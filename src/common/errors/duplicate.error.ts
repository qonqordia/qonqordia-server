export class DuplicateError extends Error {
  public static readonly CODE = "DUPLICATE";

  public originalMessage?: string;

  constructor(entity: string, properties: Object) {
    super(DuplicateError.CODE);
    this.originalMessage = `An entity ${entity} with properties ${JSON.stringify(properties)} already exists`;
  }
}
