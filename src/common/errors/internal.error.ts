export class InternalError extends Error {
  public static readonly CODE = "INTERNAL";

  public originalMessage?: string;

  constructor(message?: string) {
    super(InternalError.CODE);
    this.originalMessage = message;
  }
}
