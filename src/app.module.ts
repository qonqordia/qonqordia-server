import { ColorController } from './presentation/http/color/color.controller';
import { AuthenticationController } from './presentation/http/authentication/authentication.controller';
import { Module } from "@nestjs/common";
// import { PresentationModule } from "./presentation/presentation.module";
import { DomainModule } from './domain/domain.module';

@Module({
  controllers: [AuthenticationController, ColorController],
  imports: [DomainModule],
})
export class AppModule {}
