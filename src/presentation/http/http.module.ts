import { AuthenticationController } from './authentication/authentication.controller';
import { Module } from "@nestjs/common";
import { DomainModule } from "../../domain/domain.module";
import { ColorController } from './color/color.controller';

@Module({
  imports: [DomainModule],
  providers: [AuthenticationController, ColorController],
})
export class HttpModule {}
