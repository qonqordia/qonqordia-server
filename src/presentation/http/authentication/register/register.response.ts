import { Authenticator } from './../../../../domain/authentication/entities/authenticator';
export type RegisterResponse = Authenticator;