import { RegisterUseCase } from "./../../../domain/authentication/use-cases/register/register.use-case";
import { RegisterResponse } from "./register/register.response";
import { RegisterRequest } from "./register/register.request";
import { LoginResponse } from "./login/login.response";
import { LoginRequest } from "./login/login.request";
import { Body, Controller, HttpCode, Post } from "@nestjs/common";
import { pipe } from "fp-ts/function";
import { LoginUseCase } from "../../../domain/authentication/use-cases/login/login.use-case";
import { toPromise } from "../../../utils/functions";

@Controller()
export class AuthenticationController {
  constructor(
    private readonly loginUseCase: LoginUseCase,
    private readonly registerUseCase: RegisterUseCase
  ) {}

  @Post("login")
  @HttpCode(200)
  async login(@Body() body: LoginRequest): Promise<LoginResponse> {
    return pipe(
      this.loginUseCase.execute({
        email: body.email,
        password: body.password,
      }),
      toPromise
    );
  }

  @Post("register")
  @HttpCode(200)
  async register(@Body() body: RegisterRequest): Promise<RegisterResponse> {
    return pipe(
      this.registerUseCase.execute({
        email: body.email,
      }),
      toPromise
    );
  }
}
