import { Color } from "../../../../domain/color/entities/color"

export type GetColorByUserIdResponse = {
  color: Color;
}