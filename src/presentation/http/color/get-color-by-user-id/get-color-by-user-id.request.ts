export type GetColorByUserIdRequestParams = {
  userId: string;
};
