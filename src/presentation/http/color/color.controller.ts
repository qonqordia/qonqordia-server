import { GetColorByUserIdRequestParams } from './get-color-by-user-id/get-color-by-user-id.request';
import { SetColorByUserIdUseCase } from "./../../../domain/color/use-cases/set-color-by-user-id/set-color-by-user-id.use-case";
import { SetColorByUserIdResponse } from "./set-color-by-user-id/set-color-by-user-id.response";
import { SetColorByUserIdRequestBody, SetColorByUserIdRequestParams } from "./set-color-by-user-id/set-color-by-user-id.request";
import { GetColorByUserIdResponse } from "./get-color-by-user-id/get-color-by-user-id.response";
import { Body, Controller, Get, HttpCode, Param, Patch } from "@nestjs/common";
import { pipe } from "fp-ts/function";
import { GetColorByUserIdUseCase } from "../../../domain/color/use-cases/get-color-by-user-id/get-color-by-user-id.use-case";
import { toPromise } from "../../../utils/functions";
import { merge } from "lodash";

@Controller()
export class ColorController {
  constructor(
    private readonly getColorByUserIdUseCase: GetColorByUserIdUseCase,
    private readonly setColorByUserIdUseCase: SetColorByUserIdUseCase
  ) {}

  @Get("/users/:userId/colors")
  @HttpCode(200)
  async getColorByUserId(
    @Param() params: GetColorByUserIdRequestParams
  ): Promise<GetColorByUserIdResponse> {
    return pipe(
      this.getColorByUserIdUseCase.execute(params),
      toPromise
    );
  }

  @Patch("/users/:userId/colors")
  async setColorByUserId(
    @Param() params: SetColorByUserIdRequestParams,
    @Body() body: SetColorByUserIdRequestBody
  ): Promise<SetColorByUserIdResponse> {
    return pipe(
      this.setColorByUserIdUseCase.execute(merge({}, body, params)),
      toPromise
    );
  }
}
