import { ColorEnum } from "./../../../../domain/color/entities/color";

export type SetColorByUserIdRequestBody = {
  color: ColorEnum;
};

export type SetColorByUserIdRequestParams = {
  userId: string;
};
