import { Color } from "../../../../domain/color/entities/color";

export type SetColorByUserIdResponse = {
  color: Color;
};
