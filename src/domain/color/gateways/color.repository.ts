import { Option } from 'fp-ts/Option';
import { TaskEither } from "fp-ts/TaskEither";
import { InternalError } from "../../../common/errors/internal.error";
import { NotFoundError } from "../../../common/errors/not-found.error";
import { Color } from "../entities/color";

export type ColorFilters = {
  userId?: string;
};

export interface ColorRepository {
  create(color: Color): TaskEither<InternalError, void>;

  update(color: Color): TaskEither<InternalError, void>;

  findOneByFilter(
    filters: ColorFilters
  ): TaskEither<InternalError | NotFoundError, Option<Color>>;
}
