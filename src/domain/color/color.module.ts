import { SetColorByUserIdUseCase } from './use-cases/set-color-by-user-id/set-color-by-user-id.use-case';
import { InfrastructureModule } from './../../infrastructure/infrastructure.module';
import { Module } from "@nestjs/common";
import { GetColorByUserIdUseCase } from './use-cases/get-color-by-user-id/get-color-by-user-id.use-case';

const useCases = [GetColorByUserIdUseCase, SetColorByUserIdUseCase];

@Module({
  exports: useCases,
  imports: [InfrastructureModule],
  providers: useCases,
})
export class ColorModule {}
