import { Color } from '../../entities/color';

export type SetColorByUserIdResult = {
  color: Color;
};