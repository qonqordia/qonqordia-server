import { merge } from "lodash";
import { Color } from "../../entities/color";
import { InMemoryColorRepository } from "../../../../infrastructure/database/color/in-memory/in-memory-color.repository";
import { SetColorByUserIdUseCase } from "./set-color-by-user-id.use-case";
import { toPromise } from "../../../../utils/functions";
import { SetColorByUserIdResult } from "./set-color-by-user-id.result";

describe("get color by user id", () => {
  const userId: string = "6b321d41-5c80-45a9-b19b-ed4f74a9be02";
  const color: Color = {
    id: "17d97670-b264-4c41-804e-c0cfed537889",
    userId,
    color: "Green",
  };
  const anotherColor: Color = {
    id: "e6cb3235-aac2-43d2-bab3-49a96c362161",
    userId: "42f2eb13-8254-4aa8-b299-cb02be2f4ac0",
    color: "Orange",
  };

  let colorRepository: InMemoryColorRepository;
  let useCase: SetColorByUserIdUseCase;

  let promise: Promise<SetColorByUserIdResult>;

  describe("when color is found", () => {
    beforeEach(() => {
      colorRepository = new InMemoryColorRepository([anotherColor, color]);
      useCase = new SetColorByUserIdUseCase(colorRepository);
      promise = toPromise(useCase.execute({ userId, color: "Red" }));
    });
    test("should update color", async () => {
      await expect(promise).resolves.toMatchObject({
        color: merge({}, color, { color: "Red" }),
      });
    });
  });

  describe("when color is not found", () => {
    beforeEach(() => {
      colorRepository = new InMemoryColorRepository([anotherColor]);
      useCase = new SetColorByUserIdUseCase(colorRepository);
      promise = toPromise(useCase.execute({ userId, color: "Red" }));
    });
    test("should create and return color", async () => {
      await expect(promise).resolves.toMatchObject({
        color: {
          id: expect.any(String),
          userId,
          color: "Red",
        },
      });
    });
  });
});
