import { InternalError } from "../../../../common/errors/internal.error";

export type SetColorByUserIdErrors = InternalError;