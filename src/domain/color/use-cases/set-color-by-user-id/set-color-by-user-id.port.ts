import { ColorEnum } from './../../entities/color';

export type SetColorByUserIdPort = {
  userId: string;
  color: ColorEnum;
};
