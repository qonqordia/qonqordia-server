import { merge } from 'lodash';
import { ColorEnum } from './../../entities/color';
import { randomUUID } from "crypto";
import { pipe } from "fp-ts/function";
import { Inject } from "@nestjs/common";
import { TaskEither } from "fp-ts/TaskEither";
import { SetColorByUserIdErrors } from "./set-color-by-user-id.errors";
import { SetColorByUserIdPort } from "./set-color-by-user-id.port";
import { SetColorByUserIdResult } from "./set-color-by-user-id.result";
import { ColorRepository } from "../../gateways/color.repository";
import { option, taskEither } from "fp-ts";
import { COLOR_REPOSITORY } from "../../../../common/constants";
import { Color } from "../../entities/color";
import { InternalError } from "../../../../common/errors/internal.error";

export class SetColorByUserIdUseCase {
  constructor(
    @Inject(COLOR_REPOSITORY)
    private readonly colorRepository: ColorRepository
  ) {}

  private createColor(userId: string, colorValue: ColorEnum): TaskEither<InternalError, Color> {
    const color: Color = {
      id: randomUUID(),
      userId,
      color: colorValue,
    };
    return pipe(
      this.colorRepository.create(color),
      taskEither.map(() => color)
    );
  }

  private updateColor(color: Color, colorValue: ColorEnum): TaskEither<InternalError, Color> {
    const updatedColor = merge({}, color, { color: colorValue });
    return pipe(
      this.colorRepository.update(updatedColor),
      taskEither.map(() => updatedColor),
    );
  }

  public execute(
    port: SetColorByUserIdPort
  ): TaskEither<SetColorByUserIdErrors, SetColorByUserIdResult> {
    return pipe(
      this.colorRepository.findOneByFilter({ userId: port.userId }),
      taskEither.chainW(
        option.fold(
          () => this.createColor(port.userId, port.color),
          (color) => this.updateColor(color, port.color)
        )
      ),
      taskEither.map((color) => ({ color }))
    );
  }
}
