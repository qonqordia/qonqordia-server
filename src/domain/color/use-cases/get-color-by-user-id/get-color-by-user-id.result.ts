import { Color } from '../../entities/color';

export type GetColorByUserIdResult = {
  color: Color;
};