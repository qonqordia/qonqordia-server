import { randomUUID } from "crypto";
import { pipe } from "fp-ts/function";
import { Inject } from "@nestjs/common";
import { TaskEither } from "fp-ts/TaskEither";
import { GetColorByUserIdErrors } from "./get-color-by-user-id.errors";
import { GetColorByUserIdPort } from "./get-color-by-user-id.port";
import { GetColorByUserIdResult } from "./get-color-by-user-id.result";
import { ColorRepository } from "../../gateways/color.repository";
import { option, taskEither } from "fp-ts";
import { COLOR_REPOSITORY } from "../../../../common/constants";
import { Color } from "../../entities/color";
import { InternalError } from "../../../../common/errors/internal.error";

export class GetColorByUserIdUseCase {
  constructor(
    @Inject(COLOR_REPOSITORY)
    private readonly colorRepository: ColorRepository
  ) {}

  private createColor(userId: string): TaskEither<InternalError, Color> {
    const color: Color = {
      id: randomUUID(),
      userId,
      color: "Red",
    };
    return pipe(
      this.colorRepository.create(color),
      taskEither.map(() => color)
    );
  }

  public execute(
    port: GetColorByUserIdPort
  ): TaskEither<GetColorByUserIdErrors, GetColorByUserIdResult> {
    return pipe(
      this.colorRepository.findOneByFilter({ userId: port.userId }),
      taskEither.chainW(
        option.fold(
          () => this.createColor(port.userId),
          (color) => taskEither.of(color)
        )
      ),
      taskEither.map((color) => ({ color }))
    );
  }
}
