import { InternalError } from "../../../../common/errors/internal.error";

export type GetColorByUserIdErrors = InternalError;