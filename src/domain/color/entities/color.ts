export type ColorEnum = "Green" | "Orange" | "Red"; 

export type Color = {
  id: string;
  userId: string;
  color: ColorEnum;
};
