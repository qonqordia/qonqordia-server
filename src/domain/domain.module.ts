import { ColorModule } from './color/color.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { Module } from "@nestjs/common";

const domainModuleList = [AuthenticationModule, ColorModule];

@Module({
  exports: domainModuleList,
  imports: domainModuleList,
})
export class DomainModule {}