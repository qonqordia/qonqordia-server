import { option } from "fp-ts";
import { pipe } from "fp-ts/function";
import { Authenticator } from "./../../entities/authenticator";
import { randomUUID } from "crypto";
import { RegisterResult } from "./register.result";
import { TaskEither } from "fp-ts/TaskEither";
import { RegisterPort } from "./register.port";
import { AuthenticatorRepository } from "../../gateways/authenticator.repository";
import { Inject } from "@nestjs/common";
import { AUTHENTICATOR_REPOSITORY } from "../../../../common/constants";
import { RegisterErrors } from "./register.errors";
import { taskEither } from "fp-ts";
import bcrypt from "bcrypt";
import { toTaskEither } from "../../../../utils/functions";
import { InternalError } from "../../../../common/errors/internal.error";

export class RegisterUseCase {
  constructor(
    @Inject(AUTHENTICATOR_REPOSITORY)
    private readonly authenticatorRepository: AuthenticatorRepository
  ) {}

  private generatePasswordHash(password): TaskEither<InternalError, string> {
    return toTaskEither(() => bcrypt.hash(password, 10));
  }

  public execute(
    port: RegisterPort
  ): TaskEither<RegisterErrors, RegisterResult> {
    return pipe(
      this.generatePasswordHash("1234"),
      taskEither.map(
        (hash) =>
          <Authenticator>{
            id: randomUUID(),
            email: port.email,
            password: option.some(hash),
            userId: option.some(randomUUID()),
          }
      ),
      taskEither.chainFirstW((authenticator) =>
        this.authenticatorRepository.create(authenticator)
      )
    );
  }
}
