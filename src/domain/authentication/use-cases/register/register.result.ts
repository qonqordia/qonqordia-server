import { Authenticator } from './../../entities/authenticator';

export type RegisterResult = Authenticator;