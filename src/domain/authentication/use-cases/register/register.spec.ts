import { option } from "fp-ts";
import { InMemoryAuthenticatorRepository } from "../../../../infrastructure/database/authenticator/in-memory/in-memory-authenticator.repository";
import { RegisterUseCase } from "./register.use-case";
import { RegisterResult } from "./register.result";
import { RegisterPort } from "./register.port";
import { toPromise } from "../../../../utils/functions";
describe("register", () => {
  let authenticatorRepository: InMemoryAuthenticatorRepository;
  let useCase: RegisterUseCase;

  let promise: Promise<RegisterResult>;
  describe("when email has not been used yet", () => {
    const port: RegisterPort = {
      email:
        "52a4c7a4-beb3-4e9c-8934-f1c33499d292@068f15d0-0c3f-476f-a3e7-681545cbae4e",
    };
    beforeEach(() => {
      authenticatorRepository = new InMemoryAuthenticatorRepository([
        {
          id: "1eb424b3-154e-48f8-94ad-0dea50046fd5",
          email:
            "4c435042-a515-4012-afd4-b37442789a5e@f3b31282-ec2b-4a2f-a500-16c177b85666",
          password: option.none,
          userId: option.none,
        },
      ]);
      useCase = new RegisterUseCase(authenticatorRepository);
      promise = toPromise(useCase.execute(port));
    });
    test("should return newly created authenticator", async () => {
      const result = await promise;

      expect(result).toEqual(
        expect.objectContaining({
          id: expect.any(String),
          email: port.email,
          password: option.some(expect.any(String)),
          userId: option.some(expect.any(String)),
        })
      );
    });
    test("should create an authenticator", async () => {
      await promise;
      expect(authenticatorRepository.authenticatorList).toHaveLength(2);
      expect(authenticatorRepository.authenticatorList).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            id: expect.any(String),
            email: port.email,
            userId: option.some(expect.any(String)),
            password: option.some(expect.any(String)),
          }),
        ])
      );
    });
  });
  describe("when email has already been used", () => {
    const port: RegisterPort = {
      email:
        "52a4c7a4-beb3-4e9c-8934-f1c33499d292@068f15d0-0c3f-476f-a3e7-681545cbae4e",
    };
    beforeEach(() => {
      authenticatorRepository = new InMemoryAuthenticatorRepository([
        {
          id: "6fe56ea5-0ca7-4209-b35b-79bf40aa4dc3",
          email: port.email,
          password: option.none,
          userId: option.none,
        },
      ]);
      useCase = new RegisterUseCase(authenticatorRepository);
      promise = toPromise(useCase.execute(port));
    });
    test("should throw error", async () => {
      await expect(promise).rejects.toThrow("DUPLICATE");
    });
    test("should not create an authenticator", async () => {
      await expect(promise).rejects.toThrow();
      expect(authenticatorRepository.authenticatorList).toHaveLength(1);
    });
  });
});
