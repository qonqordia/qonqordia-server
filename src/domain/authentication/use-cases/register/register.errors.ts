import { DuplicateError } from './../../../../common/errors/duplicate.error';
import { InternalError } from './../../../../common/errors/internal.error';

export type RegisterErrors = InternalError | DuplicateError;