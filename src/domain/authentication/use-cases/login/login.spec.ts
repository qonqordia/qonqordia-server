import { PasswordNotSetError } from "./../../errors/password-not-set.error";
import { Authenticator } from "./../../entities/authenticator";
import { option } from "fp-ts";
import { WrongPasswordError } from "./../../errors/wrong-password.error";
import { NotFoundError } from "./../../../../common/errors/not-found.error";
import { LoginUseCase } from "./login.use-case";
import { InMemoryAuthenticatorRepository } from "../../../../infrastructure/database/authenticator/in-memory/in-memory-authenticator.repository";
import { toPromise } from "../../../../utils/functions";
import { LoginResult } from "./login.result";
import bcrypt from "bcrypt";

describe("login", () => {
  const email = "66caee58-5a42-4ab0-9d82-ef02b88e4c3a@98513fd3-6c3e-4bf2-94d3-cbaecd8fc2bf";
  let authenticatorRepository: InMemoryAuthenticatorRepository;
  let useCase: LoginUseCase;

  let promise: Promise<LoginResult>;

  describe("when password has been set", () => {
    const password = "1c72dd7d-abb8-4bab-8575-77351aedfd10";
    const hashedPassword = bcrypt.hashSync(password, 1);
    const userId: string = "6bb9c92d-5966-4622-bbaf-2b9423a2f4de";

    const authenticator: Authenticator = {
      id: "f8a94e12-fe36-47ff-afdb-98e9c84aab10",
      email,
      password: option.some(hashedPassword),
      userId: option.some(userId),
    };
    authenticatorRepository = new InMemoryAuthenticatorRepository([
      authenticator,
    ]);

    describe("when credentials are ok", () => {
      beforeEach(() => {
        useCase = new LoginUseCase(authenticatorRepository);
        promise = toPromise(
          useCase.execute({
            email,
            password,
          })
        );
      });
      test("should login", async () => {
        await expect(promise).resolves.toMatchObject({
          token: expect.any(String),
          userId,
        });
      });
    });

    describe("when email is wrong", () => {
      beforeEach(() => {
        useCase = new LoginUseCase(authenticatorRepository);
        promise = toPromise(
          useCase.execute({
            email:
              "f93ee811-50d5-4ce4-a31a-964760d7dbaa@a9872c38-6914-43db-b986-88d29a3a4319",
            password,
          })
        );
      });
      test("should not login", async () => {
        await expect(promise).rejects.toThrow(NotFoundError.CODE);
      });
    });

    describe("when password is wrong", () => {
      beforeEach(() => {
        useCase = new LoginUseCase(authenticatorRepository);
        promise = toPromise(
          useCase.execute({
            email,
            password: "43c3191e-a622-4f2a-a965-1879f9a87a5d",
          })
        );
      });
      test("should not login", async () => {
        await expect(promise).rejects.toThrow(WrongPasswordError.CODE);
      });
    });
  });

  describe("when password has not been set yet", () => {
    const authenticator: Authenticator = {
      id: "f8a94e12-fe36-47ff-afdb-98e9c84aab10",
      email,
      password: option.none,
      userId: option.none,
    };
    beforeEach(() => {
      authenticatorRepository = new InMemoryAuthenticatorRepository([
        authenticator,
      ]);
      useCase = new LoginUseCase(authenticatorRepository);
      promise = toPromise(
        useCase.execute({
          email,
          password: "43c3191e-a622-4f2a-a965-1879f9a87a5d",
        })
      );
    });
    test("should throw error", async () => {
      await expect(promise).rejects.toThrow(PasswordNotSetError.CODE);
    });
  });
});
