import { CorruptedAuthenticatorError } from './../../errors/corrupted-authenticator.error';
import { env } from "./../../../../infrastructure/env";
import { WrongPasswordError } from "./../../errors/wrong-password.error";
import { AUTHENTICATOR_REPOSITORY } from "./../../../../common/constants";
import { constVoid, pipe } from "fp-ts/function";
import { AuthenticatorRepository } from "../../gateways/authenticator.repository";
import { Inject } from "@nestjs/common";
import { option, taskEither } from "fp-ts";
import { TaskEither } from "fp-ts/TaskEither";
import { LoginErrors } from "./login.errors";
import { LoginPort } from "./login.port";
import { LoginResult } from "./login.result";
import jwt from "jsonwebtoken";
import { PasswordNotSetError } from "../../errors/password-not-set.error";
import { Authenticator } from "../../entities/authenticator";
import bcrypt from "bcrypt";
import { toTaskEither } from "../../../../utils/functions";

export class LoginUseCase {
  constructor(
    @Inject(AUTHENTICATOR_REPOSITORY)
    private readonly authenticatorRepository: AuthenticatorRepository
  ) {}

  private checkPasswordMatches(
    password: string
  ): (
    authenticator: Authenticator
  ) => TaskEither<PasswordNotSetError, void> {
    return (authenticator) => pipe(
      toTaskEither(() => bcrypt.compare(password, option.toNullable(authenticator.password) ?? "")),
      taskEither.chainW(taskEither.fromPredicate(
        (doesPasswordMatch) => doesPasswordMatch,
        () => new WrongPasswordError()
      )),
      taskEither.map(constVoid),
    );
  }

  private checkPasswordHasBeenSet(): (
    authenticator: Authenticator
  ) => TaskEither<PasswordNotSetError, Authenticator> {
    return taskEither.fromPredicate(
      (authenticator) => option.isSome(authenticator.password),
      () => new PasswordNotSetError()
    );
  }

  public execute(port: LoginPort): TaskEither<LoginErrors, LoginResult> {
    return pipe(
      this.authenticatorRepository.getOneByFilter({ email: port.email }),
      taskEither.chainW(this.checkPasswordHasBeenSet()),
      taskEither.chainFirstW(this.checkPasswordMatches(port.password)),
      taskEither.chainW((authenticator) => pipe(
        authenticator.userId,
        option.fold(
          () => taskEither.left(new CorruptedAuthenticatorError()),
          (userId) => taskEither.of({
            token: jwt.sign({ userId }, env().jwt.privateKey),
            userId: userId,
          })
        )
      ))
    );
  }
}
