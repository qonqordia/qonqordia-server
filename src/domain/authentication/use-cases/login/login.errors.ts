import { NotFoundError } from './../../../../common/errors/not-found.error';
import { InternalError } from '../../../../common/errors/internal.error';
import { PasswordNotSetError } from '../../errors/password-not-set.error';

export type LoginErrors = InternalError | NotFoundError | PasswordNotSetError;