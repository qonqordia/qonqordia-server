export type LoginPort = {
  email: string;
  password: string;
};
