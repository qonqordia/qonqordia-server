export type LoginResult = {
  token: string;
  userId: string;
};