import { InfrastructureModule } from './../../infrastructure/infrastructure.module';
import { Module } from "@nestjs/common";
import { LoginUseCase } from "./use-cases/login/login.use-case";
import { RegisterUseCase } from "./use-cases/register/register.use-case";

const useCases = [LoginUseCase, RegisterUseCase];

@Module({
  exports: useCases,
  imports: [InfrastructureModule],
  providers: useCases,
})
export class AuthenticationModule {}
