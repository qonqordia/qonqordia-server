export class CorruptedAuthenticatorError extends Error {
  public static readonly CODE = "CORRUPTED_AUTHENTICATOR";

  constructor() {
    super(CorruptedAuthenticatorError.CODE);
  }
}