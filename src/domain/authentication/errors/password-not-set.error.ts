export class PasswordNotSetError extends Error {
  public static readonly CODE = "PASSWORD_NOT_SET";

  constructor() {
    super(PasswordNotSetError.CODE);
  }
}