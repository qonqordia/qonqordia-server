export class WrongPasswordError extends Error {
  public static readonly CODE = "WRONG_PASSWORD";

  constructor() {
    super(WrongPasswordError.CODE);
  }
}