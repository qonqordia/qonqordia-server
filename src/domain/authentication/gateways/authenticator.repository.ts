import { WithRequired } from '../../../utils/functions';
import { DuplicateError } from '../../../common/errors/duplicate.error';
import { Authenticator } from '../entities/authenticator';
import { NotFoundError } from '../../../common/errors/not-found.error';
import { InternalError } from '../../../common/errors/internal.error';
import { TaskEither } from 'fp-ts/TaskEither';

export type AuthenticatorFilters = {
  email: string;
} 

export interface AuthenticatorRepository {
  create(properties: WithRequired<Partial<Authenticator>, "id" | "email">): TaskEither<InternalError | DuplicateError, void>
  getOneByFilter(filter: AuthenticatorFilters): TaskEither<InternalError | NotFoundError, Authenticator>;
}