import { Option } from 'fp-ts/Option';

export type Authenticator = {
  id: string;
  email: string;
  password: Option<string>;
  userId: Option<string>;
};
