import { NestFactory } from "@nestjs/core";
import { ExpressAdapter, NestExpressApplication } from "@nestjs/platform-express";
import { json } from "body-parser";
import express from "express";
import helmet from "helmet";
import { AppModule } from "./app.module";

export async function startApplication(): Promise<NestExpressApplication> {
  const server = express();
  server.use(json({ limit: "2mb" }));
  const adapter = new ExpressAdapter(server);
  adapter.use(helmet());
  adapter.enableCors({
    // credentials: true,
    // preflightContinue: true,
  });
  const app = await NestFactory.create<NestExpressApplication>(AppModule, adapter, {
    bodyParser: true,
  });

  return app;
}