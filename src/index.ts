import { env } from './infrastructure/env';
import { startApplication } from "./app";

const start = async () => {
  const app = await startApplication();
  app.listen(env().server.port);
};

export default start();
