import swc from "unplugin-swc";
import { defineConfig } from "vitest/config";

export default defineConfig({
  test: {
    deps: { inline: ['loglevel'] },
    environment: "node",
    globalSetup: ["./test/global-setup.ts"],
    globals: true,
  },
  plugins: [swc.vite()],
});
